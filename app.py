from flask import Flask, render_template, redirect, url_for
from flask_pymongo import PyMongo
from flask import request
from  bson.objectid import ObjectId

app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://localhost:27017/todolist"
mongo = PyMongo(app)



@app.route("/")
def show_task():
    tasks = mongo.db.tasks.find({})
    # print(tasks)
    result=[]
    for alltask in tasks:
        result.append({'task': alltask['task'], 'id':alltask['_id']})
    print(result)
    return render_template('index.html', result=result)



# Inserer task dans la db
@app.route("/addTask", methods=['POST'])
def add_Task ():
    new_task = {
       "task": request.form['task']
    }

    mongo.db.tasks.insert_one(new_task)
    return redirect(url_for('show_task'))


# Supprimer un message 
@app.route("/deletTask", methods=['POST'])
def delet_task():
    id = request.form['id']
    # print(id)
    key = {'_id': ObjectId(id)}
    mongo.db.tasks.delete_one(key)
    return redirect(url_for('show_task'))

# Modifier une task
@app.route('/updateTask', methods=['POST'])
def update_Task():
   
    result = request.form['id']
    new_text = request.form['update']
    key = {'_id': ObjectId(result)}
    
    print(new_text)
    mongo.db.tasks.update_one(key, {'$set': {"task" : new_text}})
   
    return redirect(url_for('show_task'))





if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True)